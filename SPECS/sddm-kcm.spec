%global sddm_kcm_commit fe615f21637872884bec50ca7c22f9b24c269d82

Name:           sddm-kcm
Version:        0
Release:        0.7.20140114git%(echo %{sddm_kcm_commit} | cut -c-8)%{?dist}
License:        GPLv2+
Summary:        SDDM KDE configuration module
Epoch:		2 

Url:            https://github.com/sddm/sddm-kcm/
Source0:        https://github.com/sddm/sddm-kcm/archive/%{sddm_kcm_commit}.tar.gz

Patch1:         sddm-kcm-new-format.patch
Patch2:         sddm-kcm-noreturn.patch

BuildRequires:  cmake
BuildRequires:  kdelibs4-devel

Requires:       sddm
Requires:       kde-runtime%{?_kde4_version: >= %{_kde4_version}}
Obsoletes:	sddm-kcm => 5.0

%description
Graphical interface to configure SDDM using the KDE system settings.

%prep
%setup -q -n %{name}-%{sddm_kcm_commit}

%patch1 -p1 -b .format
%patch2 -p0 -b .noreturn

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%files
%doc ACKNOWLEDGEMENT CONTRIBUTORS README
%{_kde4_libdir}/kde4/kcm_sddm.so
%{_kde4_datadir}/kde4/services/kcm_sddm.desktop
%{_kde4_appsdir}/sddm-kcm/
%{_libexecdir}/kde4/kcmsddm_authhelper
%{_sysconfdir}/dbus-1/system.d/org.kde.kcontrol.kcmsddm.conf
%{_datadir}/dbus-1/system-services/org.kde.kcontrol.kcmsddm.service
%{_datadir}/polkit-1/actions/org.kde.kcontrol.kcmsddm.policy

%post

%changelog
* Tue Jan 12 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> - 0-0.7.20140114gitfe615f21
- Removing dirty fix for breeze, moved it to kde4-fedora metapackage

* Wed Jan 06 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> - 0-0.6.20140114gitfe615f21
- Initial stable release for kde4-fedora repo
- Added obsolete and epoch for removal of newer F23 packages
- Remove breeze theme from sddm.conf (dirty, but to avoid rebuild another package)

* Wed Jan 21 2015 Martin Briza <mbriza@redhat.com> - 0-0.5.20140114gitfe615f21
- Applied patch by Vincent Damewood to fix configuration file incompatibility (thanks!)
- Fixed theme listing
- Resolves: #1172276 #1173825

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.4.20140114gitfe615f21
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.3.20140114gitfe615f21
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Jan 14 2014 Martin Briza <mbriza@redhat.com> - 0-0.2.20140114gitfe615f21
- Update to the latest upstream commit (fixes theme list)

* Thu Nov 14 2013 Martin Briza <mbriza@redhat.com> - 0-0.1.20131114gitafdda33c
- Initial import
