
#if 0%{?fedora} > 17
# whether to make this the default printer manager in KDE Plasma Desktop
%global makedefault 1
#endif

Summary: Printer management for KDE
Name:    kde-print-manager
Version: 4.14.3
Release: 7%{?dist}
Epoch:   3

License: GPLv2+
URL:     https://projects.kde.org/projects/kde/kdeutils/print-manager
%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0: http://download.kde.org/%{stable}/%{version}/src/print-manager-%{version}.tar.xz
# Plasma init/upgrade script
Source1: 01-fedora-print-manager.js
Patch1:  kde-print-manager-cupsGetPPD2.patch

## upstream patches

BuildRequires: gettext
BuildRequires: kdelibs4-devel
BuildRequires: cups-devel >= 1.5.0

Requires: %{name}-libs%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires: kde-runtime%{?_kde4_version: >= %{_kde4_version}}
# currently requires local cups for majority of proper function
Requires: cups
# required for the com.redhat.NewPrinterNotification D-Bus service
Requires: system-config-printer-libs
Requires:  kde-workspace >= 3:4.11.22

%if 0%{?makedefault}
# Fedora 17 will only get upgraded to 4.9.x, Fedora 18 will not ship these.
Obsoletes: system-config-printer-kde < 7:4.10
Obsoletes: kde-printer-applet < 4.10
%endif

%description
Printer management for KDE.

%package  libs
Summary:  Runtime files for %{name}
Requires: %{name} = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: %{name}-libs%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: %{name}-libs% = %{?epoch:%{epoch}:}%{version}-%{release}

%description libs
%{summary}.


%prep
%setup -q -n print-manager-%{version}
%patch1 -p1 -b .cupsGetPPD2


%build
if [ -x %{_bindir}/plasma-dataengine-depextractor ] ; then
  plasma-dataengine-depextractor plasmoid/package
fi

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%if 0%{?makedefault}
# show print-manager plasmoid by default
install -m644 -p -D %{SOURCE1} %{buildroot}%{_kde4_appsdir}/plasma-desktop/init/01-fedora-print-manager.js
mkdir %{buildroot}%{_kde4_appsdir}/plasma-desktop/updates
ln %{buildroot}%{_kde4_appsdir}/plasma-desktop/init/01-fedora-print-manager.js %{buildroot}%{_kde4_appsdir}/plasma-desktop/updates/01-fedora-print-manager.js
%endif


%files
%{_kde4_bindir}/kde-add-printer
%{_kde4_bindir}/kde-print-queue
%{_kde4_libdir}/kde4/kcm_printer_manager.so
%{_kde4_libdir}/kde4/kded_printmanager.so
%{_kde4_libexecdir}/configure-printer
%{_kde4_libdir}/kde4/imports/org/kde/printmanager/
%{_kde4_appsdir}/plasma/plasmoids/org.kde.printmanager/
%{_kde4_appsdir}/printmanager/
%{_kde4_datadir}/kde4/services/kcm_printer_manager.desktop
%{_kde4_datadir}/kde4/services/kded/printmanager.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-printmanager.desktop
%{_datadir}/dbus-1/services/org.kde.ConfigurePrinter.service
%if 0%{?makedefault}
%{_kde4_appsdir}/plasma-desktop/*/01-fedora-print-manager.js
%endif

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files libs
# private unversioned library
%{_kde4_libdir}/libkcupslib.so
%{_kde4_libdir}/kde4/plugins/designer/printmanagerwidget.so


%changelog
* Mon Oct 24 2016 Sérgio Basto <sergio@serjux.com> - 3:4.14.3-7
- Fix FTBS with cups update

* Sat Nov 14 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 4.14.3-6
- Merged changes from Sergio's fork

* Fri Nov 13 2015 Sérgio Basto <sergio@serjux.com> - 3:4.14.3-3
- not requires a fixed release of kde-workspace

* Thu Nov 12 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 4.14.3-3
- changed Requires: kdebase-workspace to better match version 
  provided by repository

* Mon Nov 09 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 4.14.3-2
- Initial stable release for kde4-fedora repo

* Sat Nov 08 2014 Rex Dieter <rdieter@fedoraproject.org> 4.14.3-1
- 4.14.3

* Sun Oct 12 2014 Rex Dieter <rdieter@fedoraproject.org> 4.14.2-2
- backport buildfixes for cups-2.0

* Sun Oct 12 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.2-1
- 4.14.2

* Tue Sep 16 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.1-1
- 4.14.1

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.14.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Aug 14 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.0-1
- 4.14.0

* Tue Aug 05 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.97-1
- 4.13.97

* Mon Jul 14 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.3-1
- 4.13.3

* Mon Jun 09 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.2-1
- 4.13.2

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.13.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun May 11 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.1-1
- 4.13.1

* Sat Apr 12 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.0-1
- 4.13.0

* Thu Apr 03 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.97-1
- 4.12.97

* Sat Mar 22 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.95-1
- 4.12.95

* Wed Mar 19 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.90-1
- 4.12.90

* Sat Mar 01 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.3-1
- 4.12.3

* Sun Feb 02 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.2-1
- 4.12.2

* Fri Jan 10 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.1-1
- 4.12.1

* Thu Dec 19 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.12.0-1
- 4.12.0

* Sun Dec 01 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.97-1
- 4.11.97

* Thu Nov 21 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.95-1
- 4.11.95

* Sat Nov 16 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.90-1
- 4.11.90

* Sat Nov 02 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.3-1
- 4.11.3

* Sun Sep 29 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.2-1
- 4.11.2

* Wed Sep 04 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.1-1
- 4.11.1

* Thu Aug 08 2013 Than Ngo <than@redhat.com> - 4.11.0-1
- 4.11.0

* Thu Jul 25 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.97-1
- 4.10.97

* Wed Jul 24 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.95-1
- 4.10.95

* Fri Jun 28 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.90-1
- 4.10.90

* Sat Jun 01 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.4-1
- 4.10.4

* Mon May 06 2013 Than Ngo <than@redhat.com> - 4.10.3-1
- 4.10.3

* Sun Mar 31 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.2-1
- 4.10.2

* Mon Mar 11 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.1-1.1
- set %%makedefault everywhere (including f17, #711719)

* Sat Mar 02 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.1-1
- 4.10.1

* Fri Feb 01 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.0-1
- 4.10.0

* Sun Jan 20 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.9.98-1
- 4.9.98

* Fri Jan 04 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.9.97-1
- 4.9.97

* Fri Dec 21 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.95-1
- 4.9.95

* Fri Dec 14 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.90-1
- 4.9.90

* Fri Dec 14 2012 Rex Dieter <rdieter@fedoraproject.org> 0.2.0-8
- Requires: cups

* Fri Dec 14 2012 Rex Dieter <rdieter@fedoraproject.org> 0.2.0-7
- fix cups renew spam (#885541)

* Fri Nov 09 2012 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.2.0-6
- Requires: system-config-printer-libs (for com.redhat.NewPrinterNotification)

* Fri Nov 09 2012 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.2.0-5
- add missing Epoch for cups-devel dependency

* Fri Nov 09 2012 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.2.0-4
- run the plasma-dataengine-depextractor (if available)

* Fri Nov 09 2012 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.2.0-2
- make this the default printer manager for KDE Plasma Desktop on F18+ (#873746)

* Thu Aug 23 2012 Rex Dieter <rdieter@fedoraproject.org> 0.2.0-1
- 0.2.0
- BR: gettext
- simplified %%description

* Tue Aug 07 2012 Rex Dieter <rdieter@fedoraproject.org> 0.1.0-1
- first try

