%if  0%{?fedora} == 24
%define parent_name f24-kde-theme
%define rel 5
%endif
%if  0%{?fedora} == 23
%define parent_name f23-kde-theme
%define rel 0
%endif

Name:		kde4-theme
Summary:	Fedora Twenty Four KDE Theme
Version:	%{fedora}.%{rel}
Release:	2%{?dist}

License:	GPLv2+ and CC-BY-SA

# We are upstream for this package
URL:		https://fedorahosted.org/fedora-kde-artwork/
Source0:	https://fedorahosted.org/releases/f/e/fedora-kde-artwork/%{parent_name}-%{version}.tar.bz2
BuildArch:	noarch

Requires:	f%{fedora}-backgrounds-kde
Requires:       kde4-theme-core

Provides:	f%{fedora}-plasma-desktoptheme = %{version}-%{release}
Provides:	f%{fedora}-plasma-theme = %{version}-%{release}

# replace it later for F25
%if 0%{?fedora} > 24
Provides:	system-kde-theme = %{version}-%{release}
Provides:	system-ksplash-theme = %{version}-%{release}
Provides:	systesm-plasma-desktoptheme = %{version}-%{release}
Provides:	system-plasma-theme = %{version}-%{release}
%endif

%if 0%{?fedora} == 24
Provides:	system-kde-theme = %{version}-%{release}
Provides:	system-ksplash-theme = %{version}-%{release}
Provides:	system-plasma-desktoptheme = %{version}-%{release}
Provides:	system-plasma-theme = %{version}-%{release}
%endif

Source1:	twenty.three.desktop

%description
This is Fedora Twenty Four KDE Theme Artwork containing Plasma Workspaces theme.


%prep
%setup -q -n %{parent_name}-%{version}

%build

%install
%if  0%{?fedora} == 23
### Look and feel
mkdir -p %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/
cp -rp lookandfeel/* \
  %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/
mkdir -p %{buildroot}%{_datadir}/kservices5/
cp -alf %{SOURCE1} %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/metadata.desktop
cp -alf \
  %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/metadata.desktop \
  %{buildroot}%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.twenty.three.desktop

### Plasma desktoptheme's
mkdir -p %{buildroot}%{_datadir}/plasma/desktoptheme/
cp -rp desktoptheme/F23/ %{buildroot}%{_datadir}/plasma/desktoptheme/
%endif

%if  0%{?fedora} == 24
### Look and feel
mkdir -p %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/
cp -rp lookandfeel/* \
  %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/
mkdir -p %{buildroot}%{_datadir}/kservices5/
cp -alf \
  %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/metadata.desktop \
  %{buildroot}%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.twenty.four.desktop

### Plasma desktoptheme's
mkdir -p %{buildroot}%{_datadir}/plasma/desktoptheme/
cp -rp desktoptheme/F24/ %{buildroot}%{_datadir}/plasma/desktoptheme/
%endif

%files
%doc README
%license COPYING.CC-BY-SA COPYING.GPLv2
%dir %{_datadir}/plasma/
%dir %{_datadir}/plasma/desktoptheme/
%dir %{_datadir}/plasma/look-and-feel/
%dir %{_datadir}/kservices5/

%if  0%{?fedora} == 23
%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/
%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.twenty.three.desktop
%{_datadir}/plasma/desktoptheme/F23/
%endif

%if  0%{?fedora} == 24
%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/
%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.twenty.four.desktop
%{_datadir}/plasma/desktoptheme/F24/
%endif


%changelog
* Mon Sep 19 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 24.0-2
- Parametrized Requires and Provides that were referring to a fixed release version.

* Thu Sep 15 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 24.0-1
- Initial version of kde4-theme for kde4-fedora repo, derived from f24-kde-theme

