
%define stable stable

Name:           libbluedevil
Summary:        A Qt wrapper for bluez
Version:        2.1
Release:        5%{?dist}
Epoch:		2

License:        LGPLv2+
URL:            https://projects.kde.org/projects/playground/libs/libbluedevil 

%if 0%{?snap}
# git archive --format=tar --remote=git://anongit.kde.org/libbluedevil bluez5  --prefix=libbluedevil-%{version}/ | xz ...
Source0:        libbluedevil-%{version}-%{snap}.tar.xz
%else
Source0: http://download.kde.org/%{stable}/libbluedevil/%{version}%{?pre:-%{pre}}/src/libbluedevil-%{version}%{?pre:-%{pre}}.tar.xz
%endif

## upstream patches
Patch1: 0001-Adapter-Add-back-alias-and-setAlias-for-binary-compa.patch
Patch2: 0002-Delete-adapter-only-after-all-devices-from-the-adapt.patch
patch3: 0003-Only-delete-adapter-when-removed-from-m_adapters-has.patch

BuildRequires:  automoc4
BuildRequires:  cmake
BuildRequires:  pkgconfig(QtCore) pkgconfig(QtDBus)

Requires:       bluez >= 5

%description
%{name} is Qt-based library written handle all Bluetooth functionality.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
%description    devel
Development files for %{name}.


%prep
%autosetup -n libbluedevil-%{version}%{?pre:-%{pre}} -p1


%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%{_libdir}/libbluedevil.so.2*

%files devel
%doc HACKING
%{_includedir}/bluedevil/
%{_libdir}/libbluedevil.so
%{_libdir}/pkgconfig/bluedevil.pc


%changelog
* Wed Jan 13 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> - 2:2.1-5
- Initial stable release for kde4-fedora repo

* Tue May 05 2015 Rex Dieter <rdieter@fedoraproject.org> 2.1-4
- followup kded4 crasher (kde#346329)

* Tue Apr 28 2015 Rex Dieter <rdieter@fedoraproject.org> 2.1-3
- kded4 crash when resume from suspend (kde#346329)

* Tue Feb 03 2015 Rex Dieter <rdieter@fedoraproject.org> 2.1-2
- pull in upstream fix for abi break (introduced in 2.1)

* Tue Dec 23 2014 Rex Dieter <rdieter@fedoraproject.org> 2.1-1
- 2.1

* Sat Dec 13 2014 Rex Dieter <rdieter@fedoraproject.org> 2.0-1
- 2.0

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0-0.10.rc1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Mon Jul 07 2014 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.9.rc1
- drop upstream commit that causes adapter to be unpowered on every boot (#1114397, kde#337193)

* Mon Jun 30 2014 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.8.rc1
- backport recent upstream commits (#1114397)

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0-0.7.rc1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Dec 24 2013 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.6.rc1
- libbluedevil-2.0-rc1 respin

* Sat Dec 21 2013 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.5.rc1
- libbluedevil-2.0-rc1

* Fri Dec 20 2013 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.4.20131220
- libbluedevil-2.0 20131220 bluez5 branch snapshot

* Thu Dec 19 2013 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.3.20131219
- libbluedevil-2.0 20131219 bluez5 branch snapshot

* Mon Dec 09 2013 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.2.20131113
- libbluedevil-2.0 20131113 bluez5 branch snapshot

* Wed Aug 14 2013 Rex Dieter <rdieter@fedoraproject.org> 2.0-0.1.20130323
- libbluedevil-2.0-20130323 bluez5 branch snapshot

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Jun 19 2013 Rex Dieter <rdieter@fedoraproject.org> 1.9.2-4
- ExcludeArch: s390 s390x (#975736)

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 29 2012 Rex Dieter <rdieter@fedoraproject.org> 1.9.2-1
- 1.9.2

* Fri Mar 30 2012 Rex Dieter <rdieter@fedoraproject.org> 1.9.1-1
- 1.9.1

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9-0.2.20110502git
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon May 02 2011 Jaroslav Reznik <jreznik@redhat.com> - 1.9-0.1
- update to pre-release snapshot of 1.9

* Mon Mar 28 2011 Jaroslav Reznik <jreznik@redhat.com> - 1.8.1-1
- update to 1.8.1

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Nov 30 2010 Jaroslav Reznik <jreznik@redhat.com> - 1.8-3
- update to 1.8-1 (respin?)

* Wed Sep 29 2010 jkeating - 1.8-2
- Rebuilt for gcc bug 634757

* Tue Sep 21 2010 Jaroslav Reznik <jreznik@redhat.com> - 1.8-1
- update to 1.8

* Fri Aug 13 2010 Jaroslav Reznik <jreznik@redhat.com> - 1.7-1
- initial package
