Name:    dolphin-plugins
Epoch:   3
Summary: Dolphin plugins for revision control systems
Version: 16.04.2
Release: 3%{?dist}

License: GPLv2+
URL:     https://projects.kde.org/projects/kde/kdesdk/dolphin-plugins
%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0: http://download.kde.org/%{stable}/applications/%{version}/src/%{name}-%{version}.tar.xz

BuildRequires: kdelibs4-devel >= 4.14
BuildRequires: kde-baseapps-devel >= %{version}
BuildRequires: qt5-qtbase-devel >= 5.0
BuildRequires: extra-cmake-modules
BuildRequires: kf5-kxmlgui-devel
BuildRequires: kf5-ki18n-devel
BuildRequires: kf5-kdelibs4support-devel
BuildRequires: kf5-kio-devel
BuildRequires: kf5-ktexteditor-devel

%if 0%{?fedora} > 22 
BuildRequires: dolphin-devel
%endif

Requires:       dolphin >= %{version}

Conflicts:      kdesdk-common < 4.10.80
Provides:       kdesdk-dolphin-plugins = %{version}-%{release}
Obsoletes:      kdesdk-dolphin-plugins < 4.10.80

%description
Plugins for the Dolphin file manager integrating the following revision control
systems:
* Dropbox
* Git
* Subversion (SVN)
* Bazaar (Bzr)
* Mercurial (Hg)

%prep
%setup -q


%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}


%files
%{_kde4_libdir}/plugins/fileviewdropboxplugin.so
%{_kde4_libdir}/plugins/fileviewgitplugin.so
%{_kde4_libdir}/plugins/fileviewsvnplugin.so
%{_kde4_libdir}/plugins/fileviewbazaarplugin.so
%{_kde4_libdir}/plugins/fileviewhgplugin.so
%{_kde4_datadir}/config.kcfg/fileviewgitpluginsettings.kcfg
%{_kde4_datadir}/config.kcfg/fileviewsvnpluginsettings.kcfg
%{_kde4_datadir}/config.kcfg/fileviewhgpluginsettings.kcfg
%{_kde4_datadir}/kservices5/fileviewdropboxplugin.desktop
%{_kde4_datadir}/kservices5/fileviewgitplugin.desktop
%{_kde4_datadir}/kservices5/fileviewsvnplugin.desktop
%{_kde4_datadir}/kservices5/fileviewbazaarplugin.desktop
%{_kde4_datadir}/kservices5/fileviewhgplugin.desktop

%changelog
* Tue Aug 02 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 16.04.2-2
- Added few requirements to build against KF5 and Qt5
- limited BR: dolphin-devel to fedora 23+

* Tue Jul 12 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 16.04.2-1
- Updated to release 16.04.2

* Wed Dec 02 2015 Sérgio Basto <sergio@serjux.com> - 15.04.3-1
- 15.04.3
- New Epoc

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 15.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 10 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.04.2-1
- 15.04.2

* Wed May 27 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.04.1-1
- 15.04.1

* Tue Apr 14 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.04.0-1
- 15.04.0

* Sun Mar 01 2015 Rex Dieter <rdieter@fedoraproject.org> - 14.12.3-1
- 14.12.3

* Tue Feb 24 2015 Than Ngo <than@redhat.com> - 14.12.2-1
- 14.12.2

* Sat Jan 17 2015 Rex Dieter <rdieter@fedoraproject.org> - 14.12.1-1
- 14.12.1

* Sat Jan 17 2015 Rex Dieter <rdieter@fedoraproject.org> 4.14.3-2
- kde-apps cleanup

* Sat Nov 08 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.3-1
- 4.14.3

* Sun Oct 12 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.2-1
- 4.14.2

* Tue Sep 16 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.1-1
- 4.14.1

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.14.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Fri Aug 15 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.0-1
- 4.14.0

* Tue Aug 05 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.97-1
- 4.13.97

* Tue Jul 15 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.3-1
- 4.13.3

* Mon Jun 09 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.2-1
- 4.13.2

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.13.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun May 11 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.1-1
- 4.13.1

* Sat Apr 12 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.0-1
- 4.13.0

* Fri Apr 04 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.97-1
- 4.12.97

* Sat Mar 22 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.95-1
- 4.12.95

* Wed Mar 19 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.90-1
- 4.12.90

* Sun Mar 02 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.3-1
- 4.12.3

* Fri Jan 31 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.2-1
- 4.12.2

* Fri Jan 10 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.1-1
- 4.12.1

* Thu Dec 19 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.12.0-1
- 4.12.0

* Sun Dec 01 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.97-1
- 4.11.97

* Thu Nov 21 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.95-1
- 4.11.95

* Sat Nov 16 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.90-1
- 4.11.90

* Sat Nov 02 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.3-1
- 4.11.3

* Sat Sep 28 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.2-1
- 4.11.2

* Wed Sep 04 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.1-1
- 4.11.1

* Wed Aug 14 2013 Jan Grulich <jgrulich@redhat.com> - 4.11.0-1
- 4.11.0

* Wed Aug 07 2013 Jan Grulich <jgrulich@redhat.com> - 4.10.97-1
- Split off from kdesdk package
